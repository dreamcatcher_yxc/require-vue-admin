function __loadScript__(url, onLoad, onError) {
  var script = document.createElement("script");  //创建一个script标签
  script.type = "text/javascript";
  try {
    //IE浏览器认为script是特殊元素,不能再访问子节点;报错;
    script.src = url;
    script.addEventListener('load', onLoad || function () { });
  } catch (ex) {
    onError && onError(ex);
  }
  document.getElementsByTagName('head')[0].appendChild(script);
}

function AsyncRunner() {
  this.tasks = [];
};

AsyncRunner.prototype.add = function (task) {
  this.tasks.push(task);
  return this
}

AsyncRunner.prototype.prepare = function () {
  this.tasks.reverse()
  var tnext = function () {
    return function () {
      // ...;
    }
  }
  for (let task of this.tasks) {
    tnext = task(tnext)
  }
  return tnext;
}

AsyncRunner.prototype.serialRun = function() {
  var self = this;
  return new Promise(function(resolve) {
    // 在末尾添加一个已完成回调
    self.add(function(next) {
      return function() {
        resolve();
        next();
      }
    });
    self.prepare()();
  });
}

function compatibilityDetection() {
  return new Promise(function(resolve, reject) {
    if ('ActiveXObject' in window) {
      reject();
    } else {
      resolve();
    }
  });
}

/**
 * 按照 scriptUrls 的顺序记载 js 文件
 * @param {*} scriptUrls
 */
function qwSerialLoadScripts (scriptUrls) {
  return new Promise(function(resolve) {
    var runner = new AsyncRunner();

    scriptUrls.forEach(function(url) {
      runner.add(function(next) {
        return function() {
          __loadScript__(url, function() {
            next();
          });
        }
      });
    });

    runner.serialRun()
      .then(function() {
        resolve();
      });
  });
}