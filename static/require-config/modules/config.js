define([], function () {
    var process = {
        // 当前运行环境
        ENV : window.__env__
    };
    var env = '';

    switch (process.ENV) {
        case 'dev':
        case 'test':
            env = 'development';
            break;
        case 'prd':
            env = 'production';
            break;
    }

    return {
        env: env,
        version: window.__version__ || '1.0',
        timeout: window.__timeout__,
        routerMode: window.__router_mode__ || 'hash',
        alias : {
            '@': window.__app__,
            '@R': window.__app__ + '/require-config',
            '@CMP': window.__app__ + '/components',
            '@IMG': window.__app__ + '/img',
            '@UTIL': window.__app__ + '/utils',
            '@PAGE': window.__app__ + '/pages',
            '@MOCK': window.__app__ + '/mock',
            '@ROUTE': window.__app__ + '/router',
            '@STORE': window.__app__ + '/store',
            '@ASSET': window.__app__ + '/assets',
            '@LAYOUT': window.__app__ + '/layouts'
        },
        busEvents: {
            // 会话超时事件
            SESSION_TIMEOUT: 'SESSION_TIMEOUT',
            // 访问资源返回无权限异常
            NO_AUTHORITY: 'NO_AUTHORITY',
            // 多标签模式下关闭指定标签事件
            CLOSE_PAGE: 'CLOSE_PAGE_TAB',
            // 吊机工操作端事件
            M_REFERSH_ORDER: 'M_REFERSH_ORDER'
        },
        isDev: function() {
            return this.env === 'development';
        },
        isPrd: function() {
            return this.env === 'production';
        }
    };
});