define([
    'jquery',
    'async-validator'
],function (jQuery, validator) {
    /*
        包含所有可以 "混入" 组件的公共选项, 使用的时候可以选择性的导入
    */

    /**
     * 公共 mixin 选项, 如 loading
     */
    var CommonMixin = {
        props : {
            data : {
                type : [Object, Array, String, Boolean, Number],
                default() {
                    return {}
                }
            }
        },

        filters : {
            menuName: function(route) {
                return route.meta.menuName || route.meta.title || route.name;
            }
        },

        computed: {
            isLoading: function() {
                return this.qwIsLoading;
            }
        },

        data : function(){
            return {
                qwIsLoading : false
            }
        },

        methods : {
            showLoading : function() {
                this.qwIsLoading = true;
            },

            hideLoading : function() {
                this.qwIsLoading = false;
            },

            /**
             * 将 this[toArrayKey] 中的内容替换为 fromArray
             * @param {String} toArrayKey 
             * @param {Array} fromArray 
             */
            replaceArrayByArray: function(toArrayKey, fromArray) {
                var toArray = this[toArrayKey];
                if(!Array.isArray(toArray) || !Array.isArray(fromArray)) {
                    return [];
                }
                this[toArrayKey] = [];
                fromArray.forEach(item => this[toArrayKey].push(item));
            }
        }
    };

    var AntdMixin = {
        methods: {
            /**
             * this.$form
             * @param {*} opts 
             */
            mapPropsToFields: function(opts) {
                var formRef = opts.formRef;
                var dataRef = opts.dataRef;
                var keys = opts.keys||[];
                var isInclude = opts.isInclude === undefined ? opts.isInclude : true;
                var converter = opts.converter;
                var resObj = {};

                for(var key in dataRef) {
                    // 排除忽略映射的属性
                    if(keys.length > 0) {
                        if(isInclude) {
                            if(jQuery.inArray(keys, key) < 0) {
                                continue;
                            }
                        } else {
                            if(jQuery.inArray(keys, key) >= 0) {
                                continue;
                            }
                        }
                    }
                    if(!!converter) {
                        
                    }
                    resObj[key] = formRef.createFormField({
                        value: !!converter ? converter(key, dataRef[key], dataRef) : dataRef[key] 
                    });
                }
                
                return resObj;
            }
        }
    };

    /**
     * 模态框 mixin 选项
     */
    var ModalMixin = {
        props : {
            data : {
                type : [Object, Array, String, Boolean, Number],
                default : function() {
                    return {}
                }
            },

            show : {
                type : Boolean,
                required : true
            },

            // 标记当前是否是处于编辑状态, 默认为 false
            isEdit : {
                type : Boolean,
                default : false
            }
        },

        data : function() {
            return {
                qwModalReturnData : {},
                qwModalIsShow : true
            }
        },

        computed:  {
            isVisible : function() {
                return this.show;
            }
        },

        watch : {
            show : function(newVal, oldVal) {
                this.qwModalIsShow  = newVal;
            },

            qwModalIsShow : function(newVal, oldVal) {
                if(!newVal) {
                    this.$emit('update:show', newVal);

                    if(!('$hook' in this.qwModalReturnData)) {
                        this.qwModalReturnData.$hook = false;
                    }

                    this.$emit('on-close', this.qwModalReturnData);
                }
            }
        },

        methods : {
            close : function(returnData) {
                var modalReturnData = returnData||this.qwModalReturnData;
                this.qwModalReturnData = modalReturnData;
                this.qwModalIsShow = false;
            }
        }
    };

    /**
     * validator 校验规则混入
     */
    var ValidatorMixin = {
        methods: (function(jQuery, validator) {
            var newValidator = {};
            jQuery.extend(true, newValidator, validator);
            return newValidator;
        })(jQuery, validator)
    }

    return  {
        CommonMixin : CommonMixin,
        ModalMixin : ModalMixin,
        ValidatorMixin: ValidatorMixin,
        AntdMixin: AntdMixin
    }
});