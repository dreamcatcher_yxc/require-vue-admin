define([
    'config'
], function (Conf) {
    'use strict';

    function parseURL(url) {
        var a = document.createElement('a');
        a.href = url;
        return {
            source: url,
            protocol: a.protocol.replace(':', ''),
            host: a.hostname,
            port: a.port,
            query: a.search,
            params: (function () {
                var ret = {},
                    seg = a.search.replace(/^\?/, '').split('&'),
                    len = seg.length, i = 0, s;
                for (; i < len; i++) {
                    if (!seg[i]) { continue; }
                    s = seg[i].split('=');
                    ret[s[0]] = s[1];
                }
                return ret;
            })(),
            file: (a.pathname.match(/\/([^\/?#]+)$/i) || [, ''])[1],
            hash: a.hash.replace('#', ''),
            path: a.pathname.replace(/^([^\/])/, '/$1'),
            relative: (a.href.match(/tps?:\/\/[^\/]+(.+)/) || [, ''])[1],
            segments: a.pathname.replace(/^\//, '').split('/')
        };
    }

    return {
        parse: function (script, name) {
            if (Conf.env !== 'development') {
                return script;
            }
            var lines = script.split(/\r\n/);

            while (lines.length > 0) {
                var line = lines[lines.length - 1];
                if (line.trim().length === 0) {
                    lines.pop();
                }
                break;
            }

            var lastLine = lines[lines.length - 1].trim();

            if (lastLine.indexOf('//# sourceURL=') !== 0) {
                lines.push('//# sourceURL= ' + parseURL(name).path);
            }

            return lines.join('\r\n');
        }
    }
});