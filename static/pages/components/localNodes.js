define(function() {
    var defaultConf = '{"nodes":[{"name":"国内申请商标","id":"1601480744229"},{"name":"向商标局提交申请","id":"1601480756214"},{"name":"形式审查","id":"1601481517030"},{"name":"是否符合要求？","id":"1601481529813"},{"name":"不予受理","id":"1601481565031"},{"name":"限期补正","id":"1601481598410"},{"name":"补正材料之后是否符合要求？","id":"1601481650201"},{"name":"实质审查","id":"1601481668155"},{"name":"是否符合要求2？","id":"1601482048242"},{"name":"初步审定公告","id":"1601482066064"},{"name":"是否完全驳回？","id":"1601482123053"},{"name":"是否复审？","id":"1601482141672"},{"name":"删除商标","id":"1601482164717"},{"name":"是否复审2","id":"1601482879742"},{"name":"驳回复审","id":"1601606214339"},{"name":"商标评审","id":"1601606265992"},{"name":"是否有异议","id":"1601606530901"},{"name":"异议","id":"1601606560687"},{"name":"是否复审3","id":"1601606601078"},{"name":"是否复审4","id":"1601606608742"},{"name":"不予注册复审","id":"1601606865249"},{"name":"注册公告","id":"1601607046016"},{"name":"注销","id":"1601607246567"},{"name":"撤销","id":"1601607250426"},{"name":"无效宣告<商标局>","id":"1601607265111"},{"name":"是否复审5？","id":"1601607399704"},{"name":"删除商标或部分商品","id":"1601607414857"},{"name":"无效宣告复审","id":"1601607441355"},{"name":"无效宣告<商评委>","id":"1601607523342"},{"name":"是否复审6？","id":"1601607711846"},{"name":"撤销复审","id":"1601607732915"},{"name":"不服评审决定，裁判","id":"1601607878984"},{"name":"北京知识产权法院","id":"1601607954395"},{"name":"北京市高级人民法院","id":"1601607972485"}],"lines":[{"nodeId":"1601480744229","nextNodeId":"1601480756214","id":"1601481699952"},{"nodeId":"1601480756214","nextNodeId":"1601481517030","id":"1601481708500"},{"nodeId":"1601481517030","nextNodeId":"1601481529813","id":"1601481724810"},{"desc":"否","nodeId":"1601481529813","nextNodeId":"1601481565031","id":"1601481757132"},{"desc":"基本符合","nodeId":"1601481529813","nextNodeId":"1601481598410","id":"1601481782266"},{"nodeId":"1601481598410","nextNodeId":"1601481650201","id":"1601481793668"},{"desc":"否","nodeId":"1601481650201","nextNodeId":"1601481565031","id":"1601481814030"},{"desc":"是","nodeId":"1601481650201","nextNodeId":"1601481668155","id":"1601481830691"},{"desc":"是","nodeId":"1601481529813","nextNodeId":"1601481668155","id":"1601481935170"},{"nodeId":"1601481668155","nextNodeId":"1601482048242","id":"1601482198783"},{"desc":"是","nodeId":"1601482048242","nextNodeId":"1601482066064","id":"1601482231677"},{"desc":"否","nodeId":"1601482048242","nextNodeId":"1601482123053","id":"1601482250700"},{"desc":"是","nodeId":"1601482123053","nextNodeId":"1601482141672","id":"1601482299880"},{"desc":"否","nodeId":"1601482141672","nextNodeId":"1601482164717","id":"1601482388279"},{"desc":"否<部分驳回>","nodeId":"1601482123053","nextNodeId":"1601482879742","id":"1601482467870"},{"nodeId":"1601482879742","nextNodeId":"1601482066064","id":"1601606109591","desc":"否<核准部分予以公告>"},{"desc":"是","nodeId":"1601482879742","nextNodeId":"1601606214339","id":"1601606232784"},{"desc":"","nodeId":"1601606214339","nextNodeId":"1601606265992","id":"1601606291800"},{"nodeId":"1601482066064","nextNodeId":"1601606530901","id":"1601606635135"},{"desc":"是","nodeId":"1601606530901","nextNodeId":"1601606560687","id":"1601606653456"},{"desc":"不予以注册","nodeId":"1601606560687","nextNodeId":"1601606601078","id":"1601606700378"},{"desc":"是","nodeId":"1601606601078","nextNodeId":"1601606865249","id":"1601606744854"},{"desc":"部分不予注册","nodeId":"1601606560687","nextNodeId":"1601606608742","id":"1601606815590"},{"desc":"否","nodeId":"1601606601078","nextNodeId":"1601482164717","id":"1601606890595"},{"desc":"是","nodeId":"1601606608742","nextNodeId":"1601606865249","id":"1601606965306"},{"desc":"","nodeId":"1601606865249","nextNodeId":"1601606265992","id":"1601607014844"},{"desc":"否<准予部分予以公告>","nodeId":"1601606608742","nextNodeId":"1601607046016","id":"1601607110475"},{"desc":"准予注册","nodeId":"1601606560687","nextNodeId":"1601607046016","id":"1601607186559"},{"nodeId":"1601607046016","nextNodeId":"1601607246567","id":"1601607303821"},{"nodeId":"1601607046016","nextNodeId":"1601607250426","id":"1601607307237"},{"nodeId":"1601607046016","nextNodeId":"1601607265111","id":"1601607331221"},{"nodeId":"1601607265111","nextNodeId":"1601607399704","id":"1601607475094"},{"desc":"否","nodeId":"1601607399704","nextNodeId":"1601607414857","id":"1601607489586"},{"desc":"是","nodeId":"1601607399704","nextNodeId":"1601607441355","id":"1601607500726"},{"nodeId":"1601607046016","nextNodeId":"1601607523342","id":"1601607548949"},{"nodeId":"1601607441355","nextNodeId":"1601606265992","id":"1601607600812"},{"nodeId":"1601607523342","nextNodeId":"1601606265992","id":"1601607608733"},{"nodeId":"1601607250426","nextNodeId":"1601607711846","id":"1601607757798"},{"desc":"是","nodeId":"1601607711846","nextNodeId":"1601607732915","id":"1601607778795"},{"nodeId":"1601607732915","nextNodeId":"1601606265992","id":"1601607797863"},{"desc":"否","nodeId":"1601607711846","nextNodeId":"1601607414857","id":"1601607816828"},{"nodeId":"1601606265992","nextNodeId":"1601607878984","id":"1601607910556"},{"nodeId":"1601607878984","nextNodeId":"1601607954395","id":"1601607996650"},{"nodeId":"1601607954395","nextNodeId":"1601607972485","id":"1601608004753"}]}';
    var storeKey = 'local-flow-chart-data';

    function loadDataFromLocalStorage () {
        var storeVal = localStorage.getItem(storeKey);
        if(!storeVal) {
            storeVal = defaultConf;
            localStorage.setItem(storeKey, storeVal);
        }
        return JSON.parse(storeVal);
    }

    loadDataFromLocalStorage();

    function distinct(arr, identity) {
        return Array.from(new Set(arr.map(identity))).map(id => arr.find(item => identity(item) === id));
    }

    function saveData2LocalStorage(list, isNodes, isReset) {
        var realData = loadDataFromLocalStorage();
        if(isNodes) {
            if(isReset) {
                realData.nodes = list;
            } else {
                realData.nodes = distinct(realData.nodes.concat(list), node => node.id);
            }
        } else {
            if(isReset) {
                realData.lines = list;
            } else {
                realData.lines = distinct(realData.lines.concat(list), line => line.id);
            }
        }
        localStorage.setItem(storeKey, JSON.stringify(realData));
    }

    return {
        findAll: function() {
            return loadDataFromLocalStorage();
        },
        
        addNode: function(node) {
            node.id = new Date().getTime() + '';
            saveData2LocalStorage([node], true, false); 
        },

        modifyNode: function(node) {
            var allData = this.findAll();
            var nodes = allData.nodes;
            var targetNode = nodes.find(tn => tn.id === node.id);
            targetNode.name = node.name;
            saveData2LocalStorage(nodes, true, true);
        },

        addLine: function(line) {
            line.id = new Date().getTime() + '';
            saveData2LocalStorage([line], false, false); 
        },

        modifyLine: function(line) {
            var allData = this.findAll();
            var lines = allData.lines;
            var targetLine = lines.find(tn => tn.id === line.id);
            targetLine.desc = line.desc;
            targetLine.nodeId = line.nodeId;
            targetLine.nextNodeId = line.nextNodeId;
            saveData2LocalStorage(lines, false, true); 
        },

        deleteNode: function(nodeId) {
            var realData = loadDataFromLocalStorage();
            var realNodes = realData.nodes;
            var realLines = realData.lines;

            // 删除节点
            var delNodeIndex = realNodes.findIndex(node => node.id === nodeId);
            delNodeIndex >=0 && realNodes.splice(delNodeIndex, 1);
            // 删除连线
            realLines = realLines.filter(line => line.nodeId !== nodeId && line.nextNodeId !== nodeId);

            saveData2LocalStorage(realNodes, true, true);
            saveData2LocalStorage(realLines, false, true);
        },

        deleteNodes: function(nodeIds) {
            nodeIds.forEach(nodeId => {
                this.deleteNode(nodeId);
            });
        },

        deleteLines: function(lineIds) {
            var realData = loadDataFromLocalStorage();
            var realLines = realData.lines;

            // 删除连线
            lineIds.forEach(lineId => {
                realLines = realLines.filter(line => line.id !== lineId);
            });

            var newLines = realLines.filter(lineId => realLines.findIndex(line => line.id === lineId) < 0)

            saveData2LocalStorage(newLines, false, true);
        },

        reorderLine: function(lineId, isUp) {
            var realData = loadDataFromLocalStorage();
            var realLines = realData.lines;
            var realLine = realLines.find(line => line.id === lineId);
            var brothers = realLines
                .map((line, index) => ({ line: line, index: index }))
                .filter(item => item.line.nodeId === realLine.nodeId);
            var index = brothers.findIndex(item => item.line.id === realLine.id);

            if(isUp && index === 0) {
                return;
            }

            if(!isUp && index === (brothers.length - 1)) {
                return;
            }

            if(isUp) {
                let prevIndex = index - 1;
                // brothers.splice(prevIndex, 0, tArr[1]);
                realLines.splice(brothers[prevIndex].index, 1, brothers[index].line);
                // brothers.splice(index, 0, tArr[0]);
                realLines.splice(brothers[index].index, 1, brothers[prevIndex].line);
            } else {
                let nextIndex = index + 1;
                // brothers.splice(nextIndex, 0, tArr[1]);
                realLines.splice(brothers[index].index, 1, brothers[nextIndex].line);
                // brothers.splice(index, 0, tArr[0]);
                realLines.splice(brothers[nextIndex].index, 1, brothers[index].line);
            }

            saveData2LocalStorage(realLines, false, true);
        }
    };
});