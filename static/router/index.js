define([
  'vue',
  'vue-router',
  'alias!@/store/index',
  'antd',
  'axios',
  'http-vue-loader'
], function (Vue, Router, store, Antd, Axios, httpVueLoader) {
  'use strict';

  function load(url) {
    url = url.lastIndexOf('.vue') !== (url.length - 4) ? (url + '.vue') : url;
    return httpVueLoader.load(url);
  }

  var originalPush = Router.prototype.push;

  Router.prototype.push = function (location) {
    return originalPush.call(this, location).catch(err => err);
  };

  Vue.use(Router);

  var MenuView = load('@LAYOUT/MenuView'),
    PageView = load('@LAYOUT/PageView'),
    RouteView = load('@LAYOUT/RouteView');

  var router = new Router({
    routes: [
      {
        path: '/login',
        name: '登录页',
        component: load('@PAGE/login/Login'),
        invisible: true
      },
      {
        path: '/',
        name: '首页',
        component: MenuView,
        redirect: '/dashboard/workplace',
        icon: 'none',
        invisible: true,
        meta: { ignore: true },
        children: [
          {
            path: '/dashboard',
            name: '仪表板',
            component: RouteView,
            icon: 'dashboard',
            children: [
              {
                path: '/dashboard/workplace',
                name: '工作台',
                component: load('@PAGE/dashboard/WorkPlace'),
                icon: 'none'
              },
              {
                path: '/dashboard/analysis',
                name: '分析页',
                component: load('@PAGE/dashboard/Analysis'),
                icon: 'none'
              }
            ]
          },
          {
            path: '/form',
            name: '表单页',
            component: PageView,
            icon: 'form',
            children: [
              {
                path: '/form/basic',
                name: '基础表单',
                component: load('@PAGE/form/BasicForm'),
                icon: 'none'
              },
              {
                path: '/form/step',
                name: '分步表单',
                component: load('@PAGE/form/stepForm/StepForm'),
                icon: 'none'
              },
              {
                path: '/form/advanced',
                name: '高级表单',
                component: load('@PAGE/form/advancedForm/AdvancedForm'),
                icon: 'none'
              }
            ]
          },
          {
            path: '/list',
            name: '列表页',
            component: PageView,
            icon: 'table',
            children: [
              {
                path: '/list/query',
                name: '查询表格',
                component: load('@PAGE/list/QueryList'),
                icon: 'none'
              },
              {
                path: '/list/primary',
                name: '标准列表',
                component: load('@PAGE/list/StandardList'),
                icon: 'none'
              },
              {
                path: '/list/card',
                name: '卡片列表',
                component: load('@PAGE/list/CardList'),
                icon: 'none'
              },
              {
                path: '/list/search',
                name: '搜索列表',
                component: load('@PAGE/list/search/SearchLayout'),
                icon: 'none',
                children: [
                  {
                    path: '/list/search/article',
                    name: '文章',
                    component: load('@PAGE/list/search/ArticleList'),
                    icon: 'none'
                  },
                  {
                    path: '/list/search/application',
                    name: '应用',
                    component: load('@PAGE/list/search/ApplicationList'),
                    icon: 'none'
                  },
                  {
                    path: '/list/search/project',
                    name: '项目',
                    component: load('@PAGE/list/search/ProjectList'),
                    icon: 'none'
                  }
                ]
              }
            ]
          },
          {
            path: '/detail',
            name: '详情页',
            icon: 'profile',
            component: RouteView,
            children: [
              {
                path: '/detail/basic',
                name: '基础详情页',
                icon: 'none',
                component: load('@PAGE/detail/BasicDetail')
              },
              {
                path: '/detail/advanced',
                name: '高级详情页',
                icon: 'none',
                component: load('@PAGE/detail/AdvancedDetail')
              }
            ]
          },
          {
            path: '/result',
            name: '结果页',
            icon: 'check-circle-o',
            component: PageView,
            children: [
              {
                path: '/result/success',
                name: '成功',
                icon: 'none',
                component: load('@PAGE/result/Success')
              },
              {
                path: '/result/error',
                name: '失败',
                icon: 'none',
                component: load('@PAGE/result/Error')
              }
            ]
          },
          {
            path: '/exception',
            name: '异常页',
            icon: 'warning',
            component: RouteView,
            children: [
              {
                path: '/exception/404',
                name: '404',
                icon: 'none',
                component: load('@PAGE/exception/404')
              },
              {
                path: '/exception/403',
                name: '403',
                icon: 'none',
                component: load('@PAGE/exception/403')
              },
              {
                path: '/exception/500',
                name: '500',
                icon: 'none',
                component: load('@PAGE/exception/500')
              }
            ]
          },
          {
            path: '/components',
            redirect: '/components/taskcard',
            name: '小组件',
            icon: 'appstore-o',
            component: PageView,
            children: [
              {
                path: '/components/taskcard',
                name: '任务卡片',
                icon: 'none',
                component: load('@PAGE/components/TaskCard')
              },
              {
                path: '/components/palette',
                name: '颜色复选框',
                icon: 'none',
                component: load('@PAGE/components/Palette')
              },
              {
                path: '/components/flowchart',
                name: '流程图',
                icon: 'none',
                meta: {
                  title: '基于 MERMAID 实现的简易流程图，支持动态增删，注意，节点名称不能重复，尽量不要出现特殊字符。'
                },
                component: load('@PAGE/components/FlowChart')
              }
            ]
          }
        ]
      }
    ]
  });


   //每次路由改变的时候都判断用户状态是否有效, 如果有效, 则正常访问, 否则路由到登录页面.
   router.beforeEach((to, from, next) => {
    // debugger;
    // var logged = store.state.account.logged;
    // 判断权限, 暂时判断用户名
    var path = to.fullPath;
    var isLoginPath = '/login' === path;
    var notNeedAuth = ['/login', '/components/flowchart'].findIndex(tpath => path === tpath) >= 0;

    var serverLoggedProcess = function () {
      // 访问的是登录路径
      if (notNeedAuth) {
        if(isLoginPath) {
          next({ path: '/' });
        } else {
          next();
        }
      }
      // 访问的是其他路径
      else {
        next();
        store.commit('account/setLogged', false);
      }
    }

    var serverNotLoggedProcess = function () {
      store.commit('account/setLogged', false);
      if (notNeedAuth) {
        next();
      } else {
        next({ path: '/login' });
      }
    }

    // 已登录
    Axios.post('/isLogin').then(r => {
      // 服务器端也是登录状态
      // debugger
      if (r.status === 200 && r.data.code >= 0) {
        serverLoggedProcess();
      }
      // 服务器端未登录
      else {
        serverNotLoggedProcess();
      }
    }).catch(r => {
      serverNotLoggedProcess();
    });
  });

  router.afterEach(function (to, from) {
    //...
  });

  return router;
 
});